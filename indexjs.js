const backgroundButton = document.querySelector(".background-button");
const secondButton = document.querySelector(".second-button");
const content = document.querySelector(".content");
const footer = document.querySelector(".footer");
const currentBackground = localStorage.getItem("selectedBackground");
const currentFooterBackground = localStorage.getItem("selectedFooterBackground");

if (currentBackground === '#00BFFF') {
    applyBackground('#00BFFF');
}

if (currentFooterBackground === '#FFD700') {
    applyFooterBackground('#FFD700');
}

function changeBackground() {
    if (backgroundButton.classList.contains("blue-color")) {
        applyBackground("#171717");
        localStorage.removeItem("selectedBackground");
    } else {
        applyBackground("#00BFFF");
        localStorage.setItem("selectedBackground", "#00BFFF");
    }
}

function changeFooterBackground() {
    if (secondButton.classList.contains("yellow-color")) {
        applyFooterBackground("#171717");
        localStorage.removeItem("selectedFooterBackground");
    } else {
        applyFooterBackground("#FFD700");
        localStorage.setItem("selectedFooterBackground", "#FFD700");
    }
}

function applyBackground(color) {
    if (color === "#00BFFF") {
        content.style.backgroundColor = "#00BFFF";
        backgroundButton.classList.add("blue-color");
    } else {
        content.style.backgroundColor = "#171717";
        backgroundButton.classList.remove("blue-color");
    }
}

function applyFooterBackground(color) {
    if (color === "#FFD700") {
        footer.style.backgroundColor = "#FFD700";
        secondButton.classList.add("yellow-color");
    } else {
        footer.style.backgroundColor = "#171717";
        secondButton.classList.remove("yellow-color");
    }
}

backgroundButton.addEventListener("click", changeBackground);
secondButton.addEventListener("click", changeFooterBackground);









